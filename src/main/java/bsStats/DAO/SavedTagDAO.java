package bsStats.DAO;

import bsStats.Entity.SavedTag;

import java.util.List;

public interface SavedTagDAO {
    public SavedTag getTag(Long id);
    public List<SavedTag> getAllSavedTags();
    public void saveTag(SavedTag savedTag);
}
