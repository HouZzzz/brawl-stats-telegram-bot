package bsStats.DAO;

import bsStats.Communication.ClubCommunicationImpl;
import bsStats.Entity.SavedTag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class SavedTagDAOImpl implements SavedTagDAO {
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(SavedTagDAOImpl.class);
    @Override
    public SavedTag getTag(Long id) {
        LOG.debug("sender id = " + id.toString());

        if (SavedTag.getMapOfTags().containsKey(id.toString())) {
            LOG.debug("user found in database");
            Map<String, String> mapOfTags = SavedTag.getMapOfTags();
            SavedTag tag = new SavedTag(id.toString(), mapOfTags.get(id.toString()));
            return tag;
        }

        List<SavedTag> tags = getAllSavedTags();
        if (tags.isEmpty()) {
            LOG.debug("returning null");
            return null;
        }

        if (tags.stream().anyMatch(st -> st.getSaverId().equals(id.toString()))) {
            LOG.debug("user found in database");
            return tags.stream()
                    .filter(st -> st.getSaverId().equals(id.toString()))
                    .findFirst()
                    .get();
        }

        LOG.debug("returning null");
        return null;
    }

    @Override
    public void saveTag(SavedTag savedTag) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        LOG.debug("saving tag");
        List<SavedTag> result = session.createQuery("from SavedTag where saverId = " + savedTag.getSaverId() + " ", SavedTag.class).getResultList();
        LOG.debug("result = " + result);
        if (result.size() >= 1) {
            SavedTag tag = result.get(result.size() - 1);
            tag.setTag(savedTag.getTag());
            LOG.debug("saving tag: " + tag);
            session.saveOrUpdate(tag);

            session.getTransaction().commit();
            session.close();
            LOG.debug("saved");
            return;
        }
        session.saveOrUpdate(savedTag);
        session.getTransaction().commit();
        session.close();
        LOG.debug("saved");
    }

    @Override
    public List<SavedTag> getAllSavedTags() {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();

        List<SavedTag> savedTags = session.createQuery("from SavedTag ").getResultList();
        LOG.debug("all saved tags got");
        LOG.debug("saved tags list = " + savedTags);
        session.getTransaction().commit();
        session.close();

        return savedTags;
    }

    @Autowired
    public SavedTagDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        LOG.debug("savedTagDAOImpl bean created");
    }
}
