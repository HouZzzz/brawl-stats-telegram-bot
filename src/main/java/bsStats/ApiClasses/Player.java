package bsStats.ApiClasses;

import java.util.List;

public class Player {
    private String tag;
    private String name;
    private int trophies;
    private int highestTrophies;
    private int expLevel;
    private int trioVictories;
    private int soloVictories;
    private int duoVictories;
    private Club club;
    private List<Brawler> brawlers;

    // errors
    String reason;
    String message;

    public Player(String tag, String name, int trophies, int highestTrophies, int expLevel, int trioVictories, int soloVictories, int duoVictories, Club club, List<Brawler> brawlers) {
        this.tag = tag;
        this.name = name;
        this.trophies = trophies;
        this.highestTrophies = highestTrophies;
        this.expLevel = expLevel;
        this.trioVictories = trioVictories;
        this.soloVictories = soloVictories;
        this.duoVictories = duoVictories;
        this.club = club;
        this.brawlers = brawlers;
    }

    @Override
    public String toString() {
        return "Player{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", trophies=" + trophies +
                ", highestTrophies=" + highestTrophies +
                ", expLevel=" + expLevel +
                ", trioVictories=" + trioVictories +
                ", soloVictories=" + soloVictories +
                ", duoVictories=" + duoVictories +
                ", club=" + club +
                ", brawlers=" + brawlers +
                '}';
    }

    public Player() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public int getHighestTrophies() {
        return highestTrophies;
    }

    public void setHighestTrophies(int highestTrophies) {
        this.highestTrophies = highestTrophies;
    }

    public int getExpLevel() {
        return expLevel;
    }

    public void setExpLevel(int expLevel) {
        this.expLevel = expLevel;
    }

    public int get3vs3Victories() {
        return trioVictories;
    }

    public void set3vs3Victories(int trioVictories) {
        this.trioVictories = trioVictories;
    }

    public int getSoloVictories() {
        return soloVictories;
    }

    public void setSoloVictories(int soloVictories) {
        this.soloVictories = soloVictories;
    }

    public int getDuoVictories() {
        return duoVictories;
    }

    public void setDuoVictories(int duoVictories) {
        this.duoVictories = duoVictories;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public List<Brawler> getBrawlers() {
        return brawlers;
    }

    public void setBrawlers(List<Brawler> brawlers) {
        this.brawlers = brawlers;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
