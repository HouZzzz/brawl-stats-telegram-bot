package bsStats.ApiClasses;

import java.util.List;

public class Brawler {
    private int id;
    private String name;
    private int power;
    private int rank;
    private int trophies;
    private int highestTrophies;
    private List<Gear> gears;
    private List<StarPower> starPowers;
    private List<Gadget> gadgets;


    public Brawler(int id, String name, int power, int rank, int trophies, int highestTrophies, List<Gear> gears, List<StarPower> starPowers, List<Gadget> gadgets) {
        this.id = id;
        this.name = name;
        this.power = power;
        this.rank = rank;
        this.trophies = trophies;
        this.highestTrophies = highestTrophies;
        this.gears = gears;
        this.starPowers = starPowers;
        this.gadgets = gadgets;
    }

    @Override
    public String toString() {
        return "Brawler{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", power=" + power +
                ", rank=" + rank +
                ", trophies=" + trophies +
                ", highestTrophies=" + highestTrophies +
                ", gears=" + gears +
                ", starPowers=" + starPowers +
                ", gadgets=" + gadgets +
                '}';
    }

    public static int compareBy(String sortingBy, Brawler b1, Brawler b2) {
        switch (sortingBy.toLowerCase()) {
            case "power":
                return compareByPower(b1,b2);
            case "rank":
                return compareByRank(b1,b2);
            case "trophies":
                return compareByTrophies(b1,b2);
            case "maxtrophies":
                return compareByMaxTrophies(b1,b2);

            default: return 1;
        }
    }

    public static int compareByPower(Brawler b1, Brawler b2){
        return b1.getPower() - b2.getPower();
    }
    public static int compareByRank(Brawler b1, Brawler b2) {
        return b1.getRank() - b2.getRank();
    }
    public static int compareByTrophies(Brawler b1, Brawler b2) {
        return b1.getTrophies() - b2.getTrophies();
    }
    public static int compareByMaxTrophies(Brawler b1, Brawler b2) {
        return b1.getHighestTrophies() - b2.getHighestTrophies();
    }

    public Brawler() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public int getHighestTrophies() {
        return highestTrophies;
    }

    public void setHighestTrophies(int highestTrophies) {
        this.highestTrophies = highestTrophies;
    }

    public List<Gear> getGears() {
        return gears;
    }

    public void setGears(List<Gear> gears) {
        this.gears = gears;
    }

    public List<StarPower> getStarPowers() {
        return starPowers;
    }

    public void setStarPowers(List<StarPower> starPowers) {
        this.starPowers = starPowers;
    }

    public List<Gadget> getGadgets() {
        return gadgets;
    }

    public void setGadgets(List<Gadget> gadgets) {
        this.gadgets = gadgets;
    }
}
