package bsStats.ApiClasses;

public class Member {
    private String tag;
    private String name;
    private String role;
    private int trophies;

    public Member(String tag, String name, String role, int trophies) {
        this.tag = tag;
        this.name = name;
        this.role = role;
        this.trophies = trophies;
    }

    public Member() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }
}
