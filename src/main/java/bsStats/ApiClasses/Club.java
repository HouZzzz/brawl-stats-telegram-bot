package bsStats.ApiClasses;

import java.util.List;

public class Club {
    private String tag;
    private String name;
    private String description;
    private String type;
    private int requiredTrophies;
    private int trophies;
    private List<Member> members;

    public Club(String tag, String description, String type, int requiredTrophies, int trophies, List<Member> members) {
        this.tag = tag;
        this.description = description;
        this.type = type;
        this.requiredTrophies = requiredTrophies;
        this.trophies = trophies;
        this.members = members;
    }
    public Club() {
    }

    @Override
    public String toString() {
        return "Club{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", requiredTrophies=" + requiredTrophies +
                ", trophies=" + trophies +
                ", members=" + members +
                '}';
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRequiredTrophies() {
        return requiredTrophies;
    }

    public void setRequiredTrophies(int requiredTrophies) {
        this.requiredTrophies = requiredTrophies;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
