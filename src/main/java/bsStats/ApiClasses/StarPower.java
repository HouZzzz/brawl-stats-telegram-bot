package bsStats.ApiClasses;

public class StarPower {
    private int id;
    private String name;

    public StarPower(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public StarPower() {
    }

    @Override
    public String toString() {
        return "StarPower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
