package bsStats.ApiClasses;

public class EventInfo {
    private String startTime;
    private String endTime;
    private int slotId;
    private Event event;

    public EventInfo() {
    }

    public EventInfo(String startTime, String endTime, int slotId, Event event) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.slotId = slotId;
        this.event = event;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
