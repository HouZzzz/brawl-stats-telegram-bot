package bsStats.ApiClasses;

public class Gadget {
    private int id;
    private String name;

    public Gadget(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Gadget() {
    }

    @Override
    public String toString() {
        return "Gadget{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
