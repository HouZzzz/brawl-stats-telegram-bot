package bsStats.ApiClasses;

public class Event {
    private int id;
    private String mode;
    private String map;

    public Event() {
    }

    public Event(String mode, String map) {
        this.mode = mode;
        this.map = map;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
