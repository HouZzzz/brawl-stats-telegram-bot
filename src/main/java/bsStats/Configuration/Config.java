package bsStats.Configuration;

import bsStats.Entity.SavedTag;
import bsStats.Services.SavedTagServiceImpl;
import bsStats.TgBot;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
@ComponentScan("bsStats")
public class Config {

    private static Logger LOG = LoggerFactory.getLogger(Config.class);
    @Bean
    public RestTemplate restTemplate() {
        LOG.debug("rest template created");
        return new RestTemplate();
    }

    @Bean
    public TgBot bot() throws TelegramApiException {
        TgBot bot = new TgBot(new DefaultBotOptions());
        TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(bot);

        LOG.info("bot created");
        return bot;
    }

    @Bean
    public SessionFactory sessionFactory() {
        return new org.hibernate.cfg.Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(SavedTag.class)
                .buildSessionFactory();

    }

}
