package bsStats.Services;

import java.net.URISyntaxException;

public interface EventService {
    public String getEvents() throws URISyntaxException;
}
