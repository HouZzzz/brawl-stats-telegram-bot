package bsStats.Services;

import bsStats.ApiClasses.EventInfo;
import bsStats.Communication.EventCommunication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    EventCommunication eventCommunication;

    private static Logger LOG = LoggerFactory.getLogger(EventServiceImpl.class);
    @Override
    public String getEvents() throws URISyntaxException {
        List<EventInfo> events = eventCommunication.getEvents();

        String outputText = "";

        for (EventInfo event : events) {
            if (event.getSlotId() > 8) break;
            if (event.getEvent().getMode().equals("duoShowdown")) continue;
            outputText += event.getEvent().getMode() + " : " + event.getEvent().getMap() + "\n";
        }
        LOG.debug("text generated: " + outputText.substring(0,20) + "...");
        return outputText;
    }

    @Autowired
    public EventServiceImpl(EventCommunication eventCommunication) {
        this.eventCommunication = eventCommunication;
        LOG.debug("EventServiceImpl bean created");
    }
}
