package bsStats.Services;

import bsStats.ApiClasses.Club;
import bsStats.Communication.ClubCommunication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;

@Service
public class ClubServiceImpl implements ClubService {
    ClubCommunication clubCommunication;

    private static Logger LOG = LoggerFactory.getLogger(ClubServiceImpl.class);
    @Override
    public String getClubByTag(String clubTag) throws URISyntaxException {
        Club club = clubCommunication.getClubByTag(clubTag);
        String outputText = "";

        outputText += "название: " + club.getName() + "\n\n";
        outputText += "описание:\n" + club.getDescription() + "\n\n\n";
        outputText += club.getRequiredTrophies() + " кубков для входа требуется\n";
        outputText += club.getTrophies() + " кубков клуба";

        LOG.debug("text generated: " + outputText.substring(0,20) + "...");
        return outputText;
    }

    @Autowired
    public ClubServiceImpl(ClubCommunication clubCommunication) {
        this.clubCommunication = clubCommunication;
        LOG.debug("ClubServiceImpl bean created");
    }
}
