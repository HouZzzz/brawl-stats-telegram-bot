package bsStats.Services;

import bsStats.Entity.SavedTag;

import java.util.Map;

public interface SavedTagService {
    public void saveTag(Long saver_id,String tag);
    public SavedTag getTag(Long id);

    public void saveTag(SavedTag savedTag);
    public Map<String,String> getMapOfTags();
}
