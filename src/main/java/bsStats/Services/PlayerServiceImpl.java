package bsStats.Services;

import bsStats.ApiClasses.Brawler;
import bsStats.ApiClasses.Player;
import bsStats.Communication.PlayerCommunication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerServiceImpl implements PlayerService{
    PlayerCommunication playerCommunication;

    private static Logger LOG = LoggerFactory.getLogger(PlayerServiceImpl.class);

    @Override
    public String showPlayerInfo(String playerTag) throws URISyntaxException {
        Player player = playerCommunication.getPlayerByTag(playerTag);
        if (player.getReason() != null) {
            String outputText = "Возникла ошибк при обработке запроса\n";
            outputText += player.getReason();
            outputText += player.getMessage();
            return outputText;
        }
        String outputText = "";

        outputText += "✦*" + player.getName() + "* {" + player.getTag() + "}\n";
        outputText += "\uD83D\uDEE1 клуб: " + player.getClub().getName() + " {" + player.getClub().getTag() + "}\n\n";
        outputText += player.getTrophies() + " 🏆 на данный момент (" + player.getHighestTrophies() + ") максимум\n";
        outputText += player.getSoloVictories() + " одиночных побед" + "\n";
        outputText += player.getDuoVictories() + " дуо побед" + "\n";
        outputText += player.get3vs3Victories() + " побед в 3 на 3" + "\n";

        return outputText;
    }

    @Override
    public String getBrawlersInfoByPlayerTag(String playerTag, String sortMethodOrBrawlerName) throws URISyntaxException {
        List<Brawler> playerBrawlers = playerCommunication.getPlayerByTag(playerTag).getBrawlers();

        if (sortMethodOrBrawlerName != null) {
            boolean isSort = sortMethodOrBrawlerName.startsWith("sort:");

            if (isSort) {
                String sortMethod = sortMethodOrBrawlerName.replaceFirst("sort:","");

                playerBrawlers = playerBrawlers.stream()
                        .sorted((b1,b2) -> Brawler.compareBy(sortMethod,b2,b1))
                        .limit(5)
                        .collect(Collectors.toList());
            } else {
                playerBrawlers = playerBrawlers.stream()
                        .filter(b -> b.getName().equals(sortMethodOrBrawlerName.toUpperCase()))
                        .collect(Collectors.toList());
            }
        } else {
            playerBrawlers = playerBrawlers.stream()
                    .sorted((b1,b2) -> b2.getTrophies() - b1.getTrophies())
                    .limit(5)
                    .collect(Collectors.toList());
        }


        String outputText = "";

        for (Brawler brawler : playerBrawlers) {
            outputText += brawler.getName() + "\n";
            outputText += "\t\t*" + brawler.getTrophies() + " *кубков сейчас\n";
            outputText += "\t\t*" + brawler.getHighestTrophies() + " *кубков максимум\n";
            outputText += "\t\t*" + brawler.getRank() + " *ранг\n";
            outputText += "\t\t*" + brawler.getPower() + " *сила\n\n";
        }
        LOG.debug("text generated: " + outputText.substring(0,20) + "...");
        return outputText;
    }

    @Override
    public Player getPlayerByTag(String playerTag) throws URISyntaxException {
        return playerCommunication.getPlayerByTag(playerTag);
    }

    @Autowired
    public PlayerServiceImpl(PlayerCommunication playerCommunication) {
        this.playerCommunication = playerCommunication;
        LOG.debug("PlayerServiceImpl bean created");
    }
}
