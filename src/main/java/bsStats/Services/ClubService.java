package bsStats.Services;

import bsStats.ApiClasses.Club;

import java.net.URISyntaxException;

public interface ClubService {
    public String getClubByTag(String clubTag) throws URISyntaxException;
}
