package bsStats.Services;

import bsStats.DAO.SavedTagDAO;
import bsStats.DAO.SavedTagDAOImpl;
import bsStats.Entity.SavedTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SavedTagServiceImpl implements SavedTagService {
    SavedTagDAO savedTagDAO;

    private static Logger LOG = LoggerFactory.getLogger(SavedTagServiceImpl.class);

    @Override
    public void saveTag(Long saver_id, String tag) {
        System.out.println("дошел до service save tag");

        SavedTag.getMapOfTags().put(saver_id.toString(),tag);

        SavedTag savedTag = savedTagDAO.getTag(saver_id);
        System.out.println(savedTag == null);
        if (savedTag == null) {
            System.out.println("saving new tag");
            savedTagDAO.saveTag(new SavedTag(saver_id.toString(),tag));
        } else {
            savedTag.setTag(tag);
            savedTagDAO.saveTag(savedTag);
        }

    }

    @Override
    public SavedTag getTag(Long id) {
        return savedTagDAO.getTag(id);
    }

    @Override
    public void saveTag(SavedTag savedTag) {
        String key = savedTag.getSaverId();
        String value = savedTag.getTag();

        SavedTag.getMapOfTags().put(key,value);
        savedTagDAO.saveTag(savedTag);
    }

    @Override
    public Map<String, String> getMapOfTags() {
        List<SavedTag> allSavedTags = savedTagDAO.getAllSavedTags();
        Map<String,String> mapOfTags = new HashMap<>();

        allSavedTags.forEach(st -> mapOfTags.put(st.getSaverId(),st.getTag()));
        return mapOfTags;
    }

    @Autowired
    public SavedTagServiceImpl(SavedTagDAO savedTagDAO) {
        this.savedTagDAO = savedTagDAO;
        SavedTag.setMapOfTags(getMapOfTags());
        LOG.debug("SavedTagServiceImpl bean created");
    }
}
