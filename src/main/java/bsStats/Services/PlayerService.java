package bsStats.Services;

import bsStats.ApiClasses.Player;

import java.net.URISyntaxException;

public interface PlayerService {
    public String showPlayerInfo(String playerTag) throws URISyntaxException;
    public Player getPlayerByTag(String playerTag) throws URISyntaxException;
    public String getBrawlersInfoByPlayerTag(String playerTag,String sortMethodOrBrawlerName) throws URISyntaxException;
}
