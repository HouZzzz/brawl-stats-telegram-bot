package bsStats.Communication;

import bsStats.ApiClasses.Club;
import org.springframework.stereotype.Repository;

import java.net.URISyntaxException;

public interface ClubCommunication {
    public Club getClubByTag(String clubTag) throws URISyntaxException;
}
