package bsStats.Communication;

import bsStats.ApiClasses.Club;
import bsStats.ApiClasses.EventInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Repository
public class EventCommunicationImpl implements EventCommunication {

    private RestTemplate restTemplate;
    private String bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYzN2U0MjdjLWM3MGItNDFjMi1iMjRiLTBmOGRiMjcwMTdmYSIsImlhdCI6MTY2ODE5NzAxMywic3ViIjoiZGV2ZWxvcGVyLzViMjNmZDQxLWU2MTktNTcwMi02YjE4LWY5YTJkMDUwMzBkNSIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTc4LjIxOS40Ni4xMjEiXSwidHlwZSI6ImNsaWVudCJ9XX0.KJIAB2kH3Zs4jFbmgUuApTJmltkLSZR_A_Af9P7TJTWj3kNhhFWOUZ7keL3gwS8L6nQW33FM7lU_samnlrjZWA";
    private String URL = "https://api.brawlstars.com/v1/events/rotation";

    private HttpHeaders headers = new HttpHeaders();
    private HttpEntity<String> request;
    private Logger LOG = LoggerFactory.getLogger(EventCommunicationImpl.class);

    @Override
    public List<EventInfo> getEvents() throws URISyntaxException {
        LOG.debug("getting events");
        ResponseEntity<List<EventInfo>> response = restTemplate.exchange(new URI(URL), HttpMethod.GET, request, new ParameterizedTypeReference<List<EventInfo>>() {});
        LOG.debug("request sent on " + URL);
        LOG.debug("response created with body = " + response.getBody().toString().substring(0,100) + "...");

        return response.getBody();
    }

    @Autowired
    public EventCommunicationImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;

        headers.add("Authorization","Bearer " + bearerToken);
        LOG.debug("headers created");

        request = new HttpEntity<String>(headers);
        LOG.debug("request entity created");

        LOG.debug("EventCommunicationImpl bean created");
        LOG.debug("URL = " + URL);
        LOG.debug("bearer token = " + bearerToken.substring(0,40) + "...");
    }
}
