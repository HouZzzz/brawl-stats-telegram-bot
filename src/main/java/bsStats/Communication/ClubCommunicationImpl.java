package bsStats.Communication;

import bsStats.ApiClasses.Club;
import bsStats.ApiClasses.Player;
import bsStats.MessasgeHandlers.CommandsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Repository
public class ClubCommunicationImpl implements ClubCommunication {

    private RestTemplate restTemplate;
    private String bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYzN2U0MjdjLWM3MGItNDFjMi1iMjRiLTBmOGRiMjcwMTdmYSIsImlhdCI6MTY2ODE5NzAxMywic3ViIjoiZGV2ZWxvcGVyLzViMjNmZDQxLWU2MTktNTcwMi02YjE4LWY5YTJkMDUwMzBkNSIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTc4LjIxOS40Ni4xMjEiXSwidHlwZSI6ImNsaWVudCJ9XX0.KJIAB2kH3Zs4jFbmgUuApTJmltkLSZR_A_Af9P7TJTWj3kNhhFWOUZ7keL3gwS8L6nQW33FM7lU_samnlrjZWA";
    private String URL = "https://api.brawlstars.com/v1/clubs/";
    private HttpHeaders headers = new HttpHeaders();
    private HttpEntity<String> request;

    private Logger LOG = LoggerFactory.getLogger(ClubCommunicationImpl.class);

    @Override
    public Club getClubByTag(String clubTag) throws URISyntaxException {
        LOG.debug("getting club by tag");
        // editing player tag
        clubTag = "%23" + clubTag
                .replaceAll("#","")
                .toUpperCase();

        LOG.debug("tag = " + clubTag);

        ResponseEntity<Club> response = restTemplate.exchange(new URI(URL + clubTag), HttpMethod.GET, request, Club.class);
        LOG.debug("request sent on " + URL + clubTag);
        LOG.debug("response created with body = " + response.getBody().toString().substring(0,100) + "...");

        return response.getBody();
    }

    @Autowired
    public ClubCommunicationImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;

        headers.add("Authorization","Bearer " + bearerToken);
        LOG.debug("headers created");

        request = new HttpEntity<String>(headers);
        LOG.debug("request entity created");

        LOG.debug("ClubCommunicationImpl bean created");
        LOG.debug("URL = " + URL);
        LOG.debug("bearer token = " + bearerToken.substring(0,40) + "...");
    }
}
