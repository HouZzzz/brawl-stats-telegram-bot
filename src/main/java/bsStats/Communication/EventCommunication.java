package bsStats.Communication;

import bsStats.ApiClasses.EventInfo;

import java.net.URISyntaxException;
import java.util.List;

public interface EventCommunication {
    public List<EventInfo> getEvents() throws URISyntaxException;
}
