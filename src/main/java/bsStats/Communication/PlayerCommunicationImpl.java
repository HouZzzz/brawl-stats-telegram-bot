package bsStats.Communication;

import bsStats.ApiClasses.Club;
import bsStats.ApiClasses.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Repository
public class PlayerCommunicationImpl implements PlayerCommunication {
    private RestTemplate restTemplate;
    private String bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYzN2U0MjdjLWM3MGItNDFjMi1iMjRiLTBmOGRiMjcwMTdmYSIsImlhdCI6MTY2ODE5NzAxMywic3ViIjoiZGV2ZWxvcGVyLzViMjNmZDQxLWU2MTktNTcwMi02YjE4LWY5YTJkMDUwMzBkNSIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTc4LjIxOS40Ni4xMjEiXSwidHlwZSI6ImNsaWVudCJ9XX0.KJIAB2kH3Zs4jFbmgUuApTJmltkLSZR_A_Af9P7TJTWj3kNhhFWOUZ7keL3gwS8L6nQW33FM7lU_samnlrjZWA";
    private String URL = "https://api.brawlstars.com/v1/players/";
    private HttpHeaders headers = new HttpHeaders();
    private HttpEntity<String> request;

    private Logger LOG = LoggerFactory.getLogger(PlayerCommunicationImpl.class);
    @Override
    public Player getPlayerByTag(String playerTag) throws URISyntaxException {
        LOG.debug("getting player by tag");
        // editing player tag
        playerTag = "%23" + playerTag
                .replaceAll("#","")
                .toUpperCase();

        LOG.debug("tag = " + playerTag);

        ResponseEntity<Player> response = restTemplate.exchange(new URI(URL + playerTag), HttpMethod.GET, request, Player.class);
        LOG.debug("request sent on " + URL + playerTag);
        LOG.debug("response created with body = " + response.getBody().toString().substring(0,100) + "...");

        return response.getBody();
    }

    @Autowired
    public PlayerCommunicationImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;

        headers.add("Authorization","Bearer " + bearerToken);
        LOG.debug("headers created");

        request = new HttpEntity<String>(headers);
        LOG.debug("request entity created");

        LOG.debug("ClubCommunicationImpl bean created");
        LOG.debug("URL = " + URL);
        LOG.debug("bearer token = " + bearerToken.substring(0,40) + "...");
    }
}
