package bsStats.Communication;

import bsStats.ApiClasses.Club;
import bsStats.ApiClasses.Player;

import java.net.URISyntaxException;

public interface PlayerCommunication {
    public Player getPlayerByTag(String playerTag) throws URISyntaxException;
}
