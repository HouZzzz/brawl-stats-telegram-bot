package bsStats;

import bsStats.MessasgeHandlers.CommandsHandler;
import bsStats.Services.SavedTagServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;

public class TgBot extends TelegramLongPollingBot {
    @Autowired
    private CommandsHandler commandsHandler;

    private static Logger LOG = LoggerFactory.getLogger(SavedTagServiceImpl.class);
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            message.setText(message.getText().replace("@houzzbs_bot",""));

            if (message.isCommand()) {
                try {
                    String text = commandsHandler.handleCommand(message);
                    LOG.debug("text created");

                    execute(SendMessage.builder()
                            .chatId(message.getChatId().toString())
                            .text(text)
                            .replyToMessageId(message.getMessageId())
                            .parseMode(ParseMode.MARKDOWN)
                            .build());
                } catch (TelegramApiException e) {
                    System.out.println(e.getMessage());
                    throw new RuntimeException(e);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
                return;
            }
        }
//        try {
//            execute(SendPhoto.builder()
//                    .photo(new InputFile(new File("D:\\java\\BrawlStatsBot\\src\\main\\resources\\cringecat.jpg")))
//                    .chatId(update.getMessage().getChatId())
//                    .build());
//        } catch (TelegramApiException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    public String getBotUsername() {
        return "@houzzbs_bot";
    }

    public TgBot(DefaultBotOptions options) {
        super(options);
    }

    @Override
    public String getBotToken() {
        return "5631642132:AAGJuxevRuJGU_q11Tr2gHBHS8IrxuntV88";
    }


}
