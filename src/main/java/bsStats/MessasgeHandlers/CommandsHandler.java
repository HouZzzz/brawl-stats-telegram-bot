package bsStats.MessasgeHandlers;

import bsStats.ApiClasses.Player;
import bsStats.Entity.SavedTag;
import bsStats.Services.ClubService;
import bsStats.Services.EventService;
import bsStats.Services.PlayerService;
import bsStats.Services.SavedTagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.net.URISyntaxException;

@Component("cHandler")
public class CommandsHandler {
    private PlayerService playerService;

    private ClubService clubService;

    private SavedTagService savedTagService;

    private EventService eventService;

    private static Logger LOG = LoggerFactory.getLogger(CommandsHandler.class);
    public String handleCommand (Message message) throws URISyntaxException {
        LOG.info("handling command: " + message.getText());
        LOG.info("have reply = " + message.isReply());
        switch (message.getText().split(" ")[0]) {
            case "/stats": return statsCommand(message);
            case "/save": return saveCommand(message);
            case "/club": return clubCommand(message);
            case "/brawlers": return brawlersCommand(message);
            case "/events": return eventsCommand(message);
            default: return "Неверная команда";
        }
    }

    private String statsCommand(Message message) throws URISyntaxException {
        String[] splitedMessage = message.getText().split(" ");
        LOG.debug("length = " + splitedMessage.length);

        // stats
        if (splitedMessage.length == 1) {
            // stats (ответ)
            if (message.isReply()) {
                String tag = SavedTag.getMapOfTags().get(message.getReplyToMessage().getFrom().getId().toString());
                LOG.debug("found tag: " + tag);
                return playerService.showPlayerInfo(tag);
            }
            String tag = SavedTag.getMapOfTags().get(message.getFrom().getId().toString());
            LOG.debug("found tag: " + tag);
            return playerService.showPlayerInfo(tag);
        }

        // stats #tag
        if (splitedMessage.length == 2) {
            return playerService.showPlayerInfo(splitedMessage[1]);
        }
        return "Неверный запрос";
    }

    private String saveCommand(Message message) {
        // save (ответ)
        if (message.isReply()) {
            Long saverID = message.getReplyToMessage().getFrom().getId();
            String tag = message.getText().split(" ")[1]
                    .replaceAll("#", "")
                    .toUpperCase();
            tag = "#" + tag;
            LOG.debug("saver_id = " + saverID);
            LOG.debug("tag = " + tag);
            savedTagService.saveTag(saverID,tag);

            return "Тег для пользователя *@" + message.getReplyToMessage().getFrom().getUserName() + " " + tag + "* сохранен!";
        }
        // save
        Long saverID = message.getFrom().getId();
        String tag = message.getText().split(" ")[1]
                .replaceAll("#", "")
                .toUpperCase();
        tag = "#" + tag;
        savedTagService.saveTag(saverID,tag);

        return "Ваш тег *" + tag + "* сохранен!";
    }

    private String clubCommand(Message message) throws URISyntaxException {
        String[] splitedMessage = message.getText().split(" ");

        // club
        if (splitedMessage.length == 1) {
            // club (ответ)
            if (message.isReply()) {
                String tag = SavedTag.getMapOfTags().get(message.getReplyToMessage().getFrom().getId().toString());
                LOG.debug("found tag: " + tag);
                Player player = playerService.getPlayerByTag(tag);

                return clubService.getClubByTag(player.getClub().getTag());
            }
            String playerTag = SavedTag
                    .getMapOfTags()
                    .get(message.getFrom().getId().toString());

            LOG.debug("found tag: " + playerTag);
            Player player = playerService.getPlayerByTag(playerTag);

            return clubService.getClubByTag(player.getClub().getTag());
        }

        // club #tag
        if (splitedMessage.length == 2) {
            String clubTag = playerService
                    .getPlayerByTag(splitedMessage[1])
                    .getClub()
                    .getTag();

            LOG.debug("found tag: " + clubTag);
            return clubService.getClubByTag(clubTag);
        }
        return "Неверный запрос";
    }

    private String brawlersCommand(Message message) throws URISyntaxException {
        String[] splitedMessage = message.getText().split(" ");

        // brawlers
        if (splitedMessage.length == 1) {
            // brawlers (ответ)
            if (message.isReply()) {
                String tag = SavedTag.getMapOfTags().get(message.getReplyToMessage().getFrom().getId().toString());
                LOG.debug("found tag: " + tag);
                return playerService.getBrawlersInfoByPlayerTag(tag,null);
            }
            String playerTag = SavedTag
                    .getMapOfTags()
                    .get(message.getFrom().getId().toString());
            LOG.debug("found tag: " + playerTag);
            return playerService.getBrawlersInfoByPlayerTag(playerTag,null);
        }

        // brawlers #tag // brawlers {brawler} // brawlers sort:{sort}
        if (splitedMessage.length == 2) {
            String secondArg = splitedMessage[1];

            // brawler sort:{sort} // brawlers {brawler}
            if (secondArg.startsWith("sort:") || !secondArg.startsWith("#")) {
                String key = "";
                if (message.isReply()) {
                    key = message.getReplyToMessage().getFrom().getId().toString();
                } else key = message.getFrom().getId().toString();

                LOG.debug("key = " + key);
                String playerTag = SavedTag
                        .getMapOfTags()
                        .get(key);
                LOG.debug("found tag: " + playerTag);
                return playerService.getBrawlersInfoByPlayerTag(playerTag ,secondArg);
            }

            // #tag
            return playerService.getBrawlersInfoByPlayerTag(secondArg, null);
        }

        // brawlers #tag {brawler} // brawlers #tag sort:{sort}
        if (splitedMessage.length == 3) {
            String thirdArg = splitedMessage[2];

            // brawler #tag sort:{sort} // brawlers #tag {brawler}
            if (thirdArg.startsWith("sort:") || !thirdArg.startsWith("#")) {
                return playerService.getBrawlersInfoByPlayerTag(splitedMessage[1], thirdArg);
            }

        }
        return "Неверный запрос";
    }

    private String eventsCommand(Message message) throws URISyntaxException {
        return eventService.getEvents();
    }

    @Autowired
    public CommandsHandler(PlayerService playerService, ClubService clubService, SavedTagService savedTagService, EventService eventService) {
        this.playerService = playerService;
        this.clubService = clubService;
        this.savedTagService = savedTagService;
        this.eventService = eventService;
        LOG.debug("CommandsHandler bean created");
    }
}
