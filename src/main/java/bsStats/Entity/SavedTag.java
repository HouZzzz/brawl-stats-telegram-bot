package bsStats.Entity;

import bsStats.Services.ClubServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "savedtags")
public class SavedTag {
    private static Logger LOG = LoggerFactory.getLogger(SavedTag.class);
    private static Map<String,String> mapOfTags = new HashMap<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "saver_id")
    private String saverId;

    @Column(name = "tag")
    private String tag;

    public SavedTag(String saverId, String tag) {
        this.saverId = saverId;
        this.tag = tag;
    }

    public SavedTag() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSaverId() {
        return saverId;
    }

    public void setSaverId(String saverId) {
        this.saverId = saverId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public static Map<String, String> getMapOfTags() {
        return mapOfTags;
    }

    public static void setMapOfTags(Map<String, String> mapOfTags) {
        SavedTag.mapOfTags = mapOfTags;
        LOG.debug("saved tags map = " + mapOfTags);
    }

    @Override
    public String toString() {
        return "SavedTag{" +
                "id=" + id +
                ", saverId='" + saverId + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }
}
